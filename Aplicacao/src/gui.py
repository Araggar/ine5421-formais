""" This module draws the GUI """
from os.path import join, split, abspath, isfile

import PySimpleGUI as sg
import src.persistence as ps
import src.formais as formais


class GuiFormais:
    """ GUI """

    def __init__(self):
        self.path = abspath(split(__file__)[0])
        self.save_path = join(self.path, "save")

    def format_ap(self, ap_raw):
        ap_s = states, i_alphabet, s_alphabet, transitions, start, stack, accept = ap_raw.split("|")
        tr_str = ''
        for tr in transitions.split("!"):
            tr_str += '('+tr+'),'
        tr_str = tr_str[:-1]

        str_ = '('+states+'),'+'('+i_alphabet+'),'+'('+s_alphabet+'),'+'('+tr_str+'),'+'('+start+'),'+'('+stack+'),'+'('+accept+')'
        return str_

    def format_af(self, af):
        res = ""
        t_map = {}
        changed = []
        i = 1
        for k in af.keys():
            k_rm = k.strip("*").strip(">").strip("*")
            if "," in k:
                st = "X0"
                while st in list(af.keys()) or st in [x for _, x in t_map.items()]:
                    st = "X" + str(i)
                    i += 1
                changed.append(k_rm)
                t_map[k_rm] = st
                t_map[k] = k.replace(k_rm, st)
            else:
                t_map[k_rm] = k
                t_map[k] = k

        temp = {}
        for k, v in af.items():
            temp[t_map[k]] = []
            for item in v:
                if item in changed:
                    temp[t_map[k]].append(item.replace(item, t_map[item]))
                else:
                    temp[t_map[k]].append(item)
        return temp

    def save_load(self, form):
        """ Save/Load window """
        sl_path = ""
        if form == "Load":
            sl_layout = [
                [sg.T(form)],
                [sg.In(key=form)],
                [sg.FileBrowse(target=form), sg.OK()],
            ]
        else:
            sl_layout = [
                [sg.T(form)],
                [sg.In(key=form)],
                [sg.SaveAs(target=form), sg.OK()],
            ]

        sl_w = sg.Window(form).Layout(sl_layout)

        while True:
            event, values = sl_w.Read()
            if event is None or event == "Exit":
                sl_w.Close()
                return None
            if event == "OK":
                sl_path = sl_w.FindElement(form)
                if form == "Load" and isfile(sl_path.Get()):
                    sl_w.Close()
                    return sl_path
                else:
                    sl_w.Close()
                    return sl_path

    def draw_af(self, src):
        """ Formats an automata to a prettier string """
        spacing = 0
        af = ""
        for i in src[:-1].split("\n"):
            for j in i.split(" "):
                spacing = len(j) if len(j) > spacing else spacing

        fstr = ""
        for i in src.split("\n"):
            for j in i.split(" "):
                fstr += " {} ".format(j.rjust(spacing))
            fstr += "\n"
        return fstr

    def main(self):
        """ Main window """
        HOWTO = """How To:
        
Determinização:
    * Especifique as transformações separadas por espaço (- a b c) na primeira linha.
    * Especifique as transições separador por espaço (A B C), onde a primeira letra é sempre o estado atual.
    * Use '*' para indicar estados finais e '>' para indicar o estado inicial. (>S ou *X, por exemplo)

Reconhecimento de sentenças:
    * Escreva a palavra a ser testada na primeira linha.
    * Especifique as transformações separadas por espaço (- a b c) na primeira linha.
    * Especifique as transições separador por espaço (A B C), onde a primeira letra é sempre o estado atual.
    * Use '*' para indicar estados finais e '>' para indicar o estado inicial. (>S ou *X, por exemplo)

Converter AF para GR:
    * Especifique as transformações separadas por espaço (- a b c) na primeira linha.
    * Especifique as transições separador por espaço (A B C), onde a primeira letra é sempre o estado atual.
    * Use '*' para indicar estados finais e '>' para indicar o estado inicial. (>S ou *X, por exemplo)

Converter GR para AF:
    * Em cada linha, especifique inicialmente o símbolo não terminal seguido de '->' e suas produções separadas por '|'.
    Exemplo: S -> aA | bB | b

Operações de união/intersecção:
    * Especificar o 1º AF  da mesma forma que na conversão de AF para GR.
    * Adicione '---' para indicar que o 1º AF está finalizado.
    * Especificar o 2º AF da mesma forma que o primeiro.
    * Selecionar a operação desejada no menu.
"""
        menu = [
            ["File", ["Save", "Load"]],
            ["AF(N)D", ["Determinizacao", "Reconhecimento", "Converter p/ GR", "Minimizar", "União", "Intersecção",]],
            ["GR", "Converter p/ AF"],
            ["GLC", ["Forma Normal de Chomsky", "Remover Recursividade Esquerda", "Fatoração"]],
            ["AP", "Reconhecimento AP"],
        ]
        layout = [
            [sg.Menu(menu)],
            [
                sg.Multiline(key="input", font=("Courier", 12, ""), default_text=""),
                sg.Multiline(
                    size=(80, 40),
                    key="output",
                    font=("Courier", 12, ""),
                    disabled=False,
                    default_text=HOWTO,
                ),
            ],
        ]

        main_w = sg.Window("Formais").Layout(layout)
        while True:  # Event Loop
            event, values = main_w.Read()
            if event is None or event == "Exit":
                break
            elif event in ("Save", "Load"):
                # Save/Load Buttons
                sl_path = self.save_load(event)
                if sl_path:
                    if event == "Save":
                        ps.save(sl_path.Get(), main_w.FindElement("input").Get())
                    else:
                        main_w.FindElement("input").Update(
                            ps.load(sl_path.Get(), main_w.FindElement("input").Get())
                        )
            elif event == "Minimizar":
                fnt = main_w.FindElement("input").Get()
                ind = fnt.index("\n") + 1
                unreach, dead, eq = formais.af_minimize(fnt[:-1])
                hdr = " ".join([x for x in fnt[:ind] if x != "&" and x != " " and x != "\n"]) + "\n"
                
                unreach_str = ""
                for k, v in unreach.items():
                    unreach_str += "{} {}\n".format(k, " ".join(v))
                fhdr = " ".join([x for x in fnt[:ind] if x != "&" and x != " " and x != "\n"]) + "\n"
                unreach_str = hdr + unreach_str

                dead_str = ""
                for k, v in dead.items():
                    dead_str += "{} {}\n".format(k, " ".join(v))
                fhdr = " ".join([x for x in fnt[:ind] if x != "&" and x != " " and x != "\n"]) + "\n"
                dead_str = hdr + dead_str

                eq_str = ""
                for k, v in eq.items():
                    eq_str += "{} {}\n".format(k, " ".join(v))
                fhdr = " ".join([x for x in fnt[:ind] if x != "&" and x != " " and x != "\n"]) + "\n"
                eq_str = hdr + eq_str

                fstr = "Automato original:\n{}\nEliminacao inalcancaveis:\n{}\nEliminacao estados mortos:\n{}\nAutomato minimizado\n{}\n\n### Automatos para edicao ###\n{}\n{}\n{}".format(self.draw_af(fnt), self.draw_af(unreach_str), self.draw_af(dead_str), self.draw_af(eq_str), unreach_str, dead_str, eq_str)

                main_w.FindElement("output").Update(fstr)
            elif event == "Intersecção":
                fnt = main_w.FindElement("input").Get().split('---')
                ind1 = fnt[0].index("\n") + 1
                ind2 = fnt[1].index("\n") + 1
                af1_c, af2_c, hdr_u, af_u, af_min, af_int = formais.af_intersection(fnt)
                int_hdr = list(hdr_u.split(" "))
                int_hdr = int_hdr[0:1] + int_hdr[2:]

                result = ""
                for k, v in af_int.items():
                    result += "{} {}\n".format(k, " ".join(v))
                result = " ".join(int_hdr) + "\n" + result
                    
                edit = "\n### Automato para edição ###\n{}".format(result)
                result = self.draw_af(result) + edit
                main_w.FindElement("output").Update(result)
            elif event == "Determinizacao":
                # Determ. method
                fnt = main_w.FindElement("input").Get()
                ind = fnt.index("\n") + 1
                det = formais.afnd_afd(fnt[:ind], fnt[ind:-1])
                res = ""
                for k, v in det.items():
                    res += "{} {}\n".format(k, " ".join(v))
                fhdr = (
                    " ".join(
                        [x for x in fnt[:ind] if x != "&" and x != " " and x != "\n"]
                    )
                    + "\n"
                )
                res = fhdr + res

                ed_af = self.format_af(det)
                ed_str = ""
                for k, v in ed_af.items():
                    ed_str += "{} {}\n".format(k, " ".join(v))
                edhdr = (
                    " ".join(
                        [x for x in fnt[:ind] if x != "&" and x != " " and x != "\n"]
                    )
                    + "\n"
                )
                ed_str = edhdr + ed_str

                fstr = "Automato original:\n{}\nAutomato determinizado:\n{}\nAutomato determinizado equivalente p/ edicao:\n{}\n".format(
                    self.draw_af(fnt), self.draw_af(res), ed_str
                )
                main_w.FindElement("output").Update(fstr)
            elif event == "Reconhecimento":
                # Reconhecimento de AFND/AFD
                fnt = main_w.FindElement("input").Get()
                ind = fnt.index("\n") + 1
                ind2 = fnt[ind:].index("\n") + ind + 1
                original_af = self.draw_af(fnt[ind:-1])
                new_af, result, last, path, err = formais.af_handler(
                    fnt[:ind], fnt[ind:ind2], fnt[ind2:-1]
                )
                self.format_af(new_af)
                naf = ""
                for k, v in new_af.items():
                    naf += "{} {}\n".format(k, " ".join(v))
                fhdr = (
                    " ".join(
                        [
                            x
                            for x in fnt[ind:ind2]
                            if x != "&" and x != " " and x != "\n"
                        ]
                    )
                    + "\n"
                )
                naf = fhdr + naf
                naf = self.draw_af(naf)

                ed_af = self.format_af(new_af)
                ed_str = ""
                for k, v in ed_af.items():
                    ed_str += "{} {}\n".format(k, " ".join(v))
                edhdr = (
                    " ".join(
                        [
                            x
                            for x in fnt[ind:ind2]
                            if x != "&" and x != " " and x != "\n"
                        ]
                    )
                    + "\n"
                )
                ed_str = edhdr + ed_str

                if result:
                    res = "AF original:\n{}\nAF Resultante:\n{}\nAF Resultante equivalente p/ edicao:\n{}\nReconhece? - {}\nParou no estado - {}\nCaminho percorrido - {}".format(
                        original_af, naf, ed_str, result, last, " -> ".join(path)
                    )

                else:
                    res = "AF original:\n{}\nAF Resultante:\n{}\nAf Resultante equivalente p/ edicao:\n{}\nReconhece? - {}\nParou no estado - {}\nCaminho percorrido - {}\nMotivo - {}".format(
                        original_af, naf, ed_str, result, last, " -> ".join(path), err
                    )
                main_w.FindElement("output").Update(res)
            elif event == "Remover Recursividade Esquerda":
                fnt = main_w.FindElement("input").Get()
                resp, first = formais.remove_left_rec(fnt)
                str_ = first+" -> "+resp[first]+"\n"
                for k,v in resp.items():
                    if k == first:
                        continue
                    str_ += k+" -> "+v+"\n"
                main_w.FindElement("output").Update(str_)
            elif event == "Reconhecimento AP":
                fnt = main_w.FindElement("input").Get().split("\n")
                wrd = fnt[0]
                ap = fnt[1]
                resp = formais.rec_ap(ap, wrd)
                resp = "Input: "+wrd+"\nAP: "+self.format_ap(ap)+"\n\n"+"'"+wrd+"'"+" é uma palavra aceita pelo AP" if resp else "Input: "+wrd+"\nAP: "+self.format_ap(ap)+"\n\n"+"'"+wrd+"'"+" não é aceita pelo AP"
                main_w.FindElement("output").Update(resp)
            elif event == "Converter p/ GR":
                # Conversao para GR
                fnt = main_w.FindElement("input").Get()
                gr = formais.afd_gr(fnt)
                main_w.FindElement("output").Update(gr)
            elif event == "Converter p/ AF":
                font = main_w.FindElement("input").Get()
                hdr, af = formais.gr_afnd(font)
                naf = ""
                for k, v in af.items():
                    naf += "{} {}\n".format(k, " ".join(v))
                fhdr = hdr + "\n"
                naf = fhdr + naf

                edit = "\n### Automato para edição ###\n{}".format(naf)
                naf = self.draw_af(naf) + edit
                main_w.FindElement("output").Update(naf)
            elif event == "União":
                font = main_w.FindElement("input").Get().split("---")
                hdr, result = formais.af_union(font[0], font[1])
                final = ""
                for k, v in result.items():
                    final += "{} {}\n".format(k, " ".join(v))
                final_header = hdr + "\n"
                final = final_header + final

                edit = "\n### Automato para edição ###\n{}".format(final)
                final = self.draw_af(final) + edit

                main_w.FindElement("output").Update(final)
            elif event == "Forma Normal de Chomsky":
                font = main_w.FindElement("input").Get()
                glc_nullprod, glc_unitprod, glc = formais.chosmky_form(font)
                result_nullprod = ""
                for k, v in glc_nullprod.items():
                    result_nullprod += "{} -> {}\n".format(k, " | ".join(v))
                result_unitprod = ""
                for k, v in glc_unitprod.items():
                    result_unitprod += "{} -> {}\n".format(k, " | ".join(v))
                result = ""
                for k, v in glc.items():
                    result += "{} -> {}\n".format(k, " | ".join(v))
                result = "Sem produções nulas:\n{}\nSem unitárias:\n{}\nForma Normal de Chomsky:\n{}\n".format(result_nullprod, result_unitprod, result) 
                main_w.FindElement("output").Update(result)
            elif event == "Fatoração":
                font = main_w.FindElement("input").Get()
                glc_fact = formais.factorization(font)
                result = ""
                for k, v in glc_fact.items():
                    result += "{} -> {}\n".format(k, " | ".join(v))
                main_w.FindElement("output").Update(result)
        main_w.Close()


if __name__ == "__main__":
    GuiFormais().main()
