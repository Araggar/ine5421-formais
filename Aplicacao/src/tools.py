""" Generic Tools """

def parser_afnd(font):
    """ Parses DFA & NDFA """
    result = {}
    temp = [x.split(" ") for x in font.split("\n")]
    for i in temp:
        result[i[0]] = i[1:]
    return result

def parser_afnd_ep(font):
    """ Parses DFA & NDFA& """
    result = {}
    temp = [x.split(" ") for x in font.split("\n")]
    for i in temp:
        result[i[0]] = i[1:]
    return result

if __name__ == '__main__':
    TEST = """S -> A B C
A -> a aA aB
B -> b bC
C -> cA"""
    TEST_AFD = """>*A B B C
B - - A
*C B B -"""

#    print(parser_er(TEST))
    print(parser_afnd(TEST_AFD))








