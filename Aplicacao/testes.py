import src.formais as src
import src.tools as tl

print("Start")


GLC = """S -> ASB
A -> aAS | a | &
B -> SbS | A | bb
"""

GLC_2 = """S -> aASA | AaB
A -> aS | a | Sa
B -> b | &
"""

AP_1 = """A0,B1,B2|a,b|Q,P|A0,a,Q,B1,P!B1,b,P,B1,!B1,&,P,B1,|A0|Q|B1,B2"""

print(src.rec_ap(AP_1, 'ab'))

