import src.tools as tl
from sys import exit
# import tools as tl


from string import ascii_letters, ascii_lowercase, digits, ascii_uppercase
from random import choice
from copy import deepcopy

def sanitize_af(src):
    """ Cleans up AFD entries"""
    snt = {}
    for key, value in src.items():
        key = key.strip(">").strip("*").strip(">")
        snt[key] = value
    return snt


def epslon_handler(hdr, src):
    """ Handles &-closure operations """
    # Creates the initial dictionary for &-closure
    ind = hdr.index("&") - 2
    fecho = {}
    temp = tl.parser_afnd(src)
    temp = sanitize_af(temp)

    for k in temp:
        aux = [k]
        for s in temp[k][ind].split():
            aux.append(temp[k][ind]) if temp[k][ind] != "-" and temp[k][
                ind
            ] not in "".join(aux) else None
        fecho[k] = ",".join(sorted(aux))

    for k, v in fecho.items():
        tmp_wrd = []
        for i in fecho:
            for wrd in fecho[k].split(','):
                if i == wrd:
                    tmp_wrd.extend(wrd.replace(i, fecho[i]).split(','))
        
        new_wrd = ','.join(sorted(list(set(tmp_wrd))))
        fecho[k] = new_wrd #sorted(list(set(fecho[k].replace(i, fecho[i]).split(","))))

    for k, v in fecho.items():
        tmp_wrd = []
        for i in fecho:
            for wrd in fecho[k].split(','):
                if i == wrd:
                    tmp_wrd.extend(wrd.replace(i, fecho[i]).split(','))
        
        new_wrd = ','.join(sorted(list(set(tmp_wrd))))
        fecho[k] = new_wrd #sorted(list(set(fecho[k].replace(i, fecho[i]).split(","))))
    
    ## Applies Fecho 
    for k, v in temp.items():
        for j in range(len(hdr.split(" ")) - 1):
            tmp_wrd = []
            for i in fecho:
                for wrd in temp[k][j].split(','):
                    if i == wrd:
                        tmp_wrd.extend(wrd.replace(i, fecho[i]).split(','))
            new_wrd = ','.join(sorted(list(set(tmp_wrd)))) if tmp_wrd else '-'
            temp[k][j] = new_wrd
    
    for k, v in temp.items():
        for j in range(len(hdr.split(" ")) - 1):
            tmp_wrd = []
            for i in fecho:
                for wrd in temp[k][j].split(','):
                    if i == wrd:
                        tmp_wrd.extend(wrd.replace(i, fecho[i]).split(','))
            new_wrd = ','.join(sorted(list(set(tmp_wrd)))) if tmp_wrd else '-'
            temp[k][j] = new_wrd

    # Remove unused entries
    final = {}
    for k in temp:
        final[k] = []
        for i in range(len(hdr.split(" ")) - 1):
            if i != ind:
                final[k].append(",".join(sorted(list(set(temp[k][i].split(","))))))
    
    return final, sorted(list(set([v for k, v in fecho.items()]))), fecho


def afnd_afd(hdr, src):
    """ Transforms NDFA in DFA """
    # Checks if the AFND has & transitions
    if "&" in hdr:
        return afnd_afd_eps(hdr, src)
    else:
        return afnd_afd_simple(hdr, src)


def afnd_afd_eps(hdr, src):
    """ Transforms NDFA in DFA """
    # Parses the AFND into a dictionary
    src1 = tl.parser_afnd(src)
    # Stores the original keys
    original_states = list(sanitize_af(src1).keys())

    # Find acceptance/start states
    raw_accept = [y for y in src1.keys() if "*" in y]
    raw_start = [y for y in src1.keys() if ">" in y]
    accept = [y.strip('*').strip('>').strip('*') for y in raw_accept]
    start = [y.strip('*').strip('>').strip('*') for y in raw_start]

    # Handles &-transitions
    src2, new_states, fecho = epslon_handler(hdr, src)
    start[0] = fecho[start[0]]
    starting_states = [v for _, v in fecho.items()]

    # Builds the AFD
    n = len(hdr.split(" ")) - 2
    while new_states:
        temp = []
        state = new_states.pop()
        if state not in src2.keys():
            src2[state] = []
            decomp = sorted(set([x for x in state.split(",") if x != "-"]))
            for i in range(n):
                temp = []
                for s in decomp:
                    temp.append(src2[s][i]) if src2[s][i] != "-" and src2[s][
                        i
                    ] not in "".join(temp) else None
                if not temp:
                    src2[state].append("-")
                else:
                    src2[state].append(",".join(sorted(set(",".join(temp).split(",")))))
        new_states.extend(
            [
                x
                for x in src2[state]
                if x not in src2.keys() and x not in new_states and x != "-"
            ]
        )
    
    # Mark initial/acceptance states
    final = {}
    for k, v in src2.items():
        if k in original_states and k not in starting_states:
            continue
        temp = []
        if k in start:
            temp.append(">")
        if any(k.find(x) > -1 for x in accept):
            temp.append("*")
        temp.append(k)
        final["".join(temp)] = src2[k]
    
    return final


def afnd_afd_simple(hdr, src):
    """ Transforms NDFA in DFA """
    # Parses the AFND into a dictionary for handling
    src1 = tl.parser_afnd(src)

    # Find accpetance/start states
    raw_accept = [y for y in src1.keys() if "*" in y]
    raw_start = [y for y in src1.keys() if ">" in y]
    accept = [y.strip('*').strip('>').strip('*') for y in raw_accept]
    start = [y.strip('*').strip('>').strip('*') for y in raw_start]

    # Cleansup clutter
    src2 = sanitize_af(src1)
    # Builds the AFD
    new_states = list(src2.keys())
    n = len(src2[start[0]])
    
    while new_states:
        temp = []
        state = new_states.pop()
        if state not in src2.keys():
            src2[state] = []
            decomp = [x for x in state.split(",") if x != "-"]
            for i in range(n):
                temp = []
                for s in decomp:
                    temp.append(src2[s][i]) if src2[s][i] != "-" and src2[s][
                        i
                    ] not in "".join(temp) else None
                if not temp:
                    src2[state].append("-")
                else:
                    src2[state].append(",".join(sorted(set(",".join(temp).split(",")))))
        new_states.extend(
            [
                x
                for x in src2[state]
                if x not in src2.keys() and x not in new_states and x != "-"
            ]
        )

    # Mark initial/acceptance states
    final = {}
    for k, v in src2.items():
        temp = []
        if k in start:
            temp.append(">")
        if any(k.find(x) > -1 for x in accept):
            temp.append("*")
        temp.append(k)
        final["".join(temp)] = src2[k]

    return final


def af_handler(word, hdr, src):
    """ Used to process words into AFD/AFND """
    # Transforms the AFND into a AFD if applicable
    afd = afnd_afd(hdr, src)
    # Creates a token map Token -> Index
    t_map = {}
    for i, t in enumerate(hdr[:-1].split(" ")):
        t_map[t] = i - 2

    # Finds Accpetance/Starting States
    raw_accept = [y for y in afd.keys() if "*" in y]
    raw_start = [y for y in afd.keys() if ">" in y]

    accept = []
    start = []

    for i in raw_accept:
        i = i.strip(">").strip("*").strip(">")
        accept.append(i)
    
    for i in raw_start:
        i = i.strip(">").strip("*").strip(">")
        start.append(i)

    # Cleans up the dictionary for better use
    s_afd = sanitize_af(afd)

    # Walks the AFD
    state = start[0]
    path = [start[0]]
    for c in word[:-1]:
        try:
            state = s_afd[state][t_map[c]]
            path.append(state)
        except Exception as err:
            # Throws an Exception when using an invalid transition/token
            return afd, False, state, path, "Transicao/Estado inválido {}".format(err)

    # Checks if the last state was an acceptance state and return the data
    if state in accept:
        return afd, True, state, path, None
    
    return afd, False, state, path, "Estado final nao eh de aceitacao."


def afd_gr(font):
    """ Transforms AF to GR """
    src = tl.parser_afnd(font[:-1])

    # Find initial/final states.
    raw_start = [y for y in src.keys() if ">" in y]

    start = []
    for i in raw_start:
        i = i.strip(">").strip("*").strip(">")
        start.append(i)

    raw_accept = [y for y in src.keys() if "*" in y]

    final_states = []
    for i in raw_accept:
        i = i.strip(">").strip("*").strip(">")
        final_states.append(i)

    # Cleans up the source
    newSrc = sanitize_af(src)
    # Separates header and transitions
    hdr, states = [x for x in newSrc["-"] if x != ""], list(newSrc.keys())[1:]

    # Creates a transition map
    t_map = {}
    for i, t in enumerate(hdr):
        t_map[t] = i

    gr = ""

    # Finds the initial state and compute the first GR line
    for state in states:
        if state not in start:
            continue
        temp_tr = []
        gr += state.upper() + " -> "
        for tr in hdr:
            transitionsTo = newSrc[state][t_map[tr]]
            if transitionsTo != "-":
                if transitionsTo in final_states:
                    temp_tr.append(tr)
                temp_tr.append(tr + transitionsTo.upper())

        gr += " | ".join(set(temp_tr))
        gr += "\n"
        break

    # Computes every other line
    for state in states:
        if state in start:
            continue
        temp_tr = []
        gr += state.upper() + " -> "
        for tr in hdr:
            transitionsTo = newSrc[state][t_map[tr]]
            if transitionsTo != "-":
                if transitionsTo in final_states:
                    temp_tr.append(tr)
                temp_tr.append(tr + transitionsTo.upper())

        gr += " | ".join(set(temp_tr))
        gr += "\n"

    # If the AFD accepts &, add a new first line that accepts it
    if start[0] in final_states:
        gr = "S0 ->" + gr.split("\n")[0].split("->")[1] + " | &\n" + gr

    return gr


def gr_afnd(font):
    font = font[:-1]
    alph = [x for x in ascii_lowercase if x in font]

    # Also check for numbers
    alph += [str(i) for i in range(0, 10) if str(i) in font]

    t_map = {}
    for i, k in enumerate(alph):
        t_map[k] = i

    entries = []
    for i in font.split("\n"):
        entries.append(i.split(" -> ")[0])

    af = {}
    for ind, i in enumerate(font.split("\n")):
        brk = i.split(" -> ")
        k, v = brk[0], brk[1].split(" | ")
        temp = [[] for _ in alph]
        for j in alph:
            aux = []
            for st in [x[1:] for x in v if x.find(j) > -1]:
                aux.append(st) if st else aux.append("X")

            if aux:
                temp[t_map[j]] = ",".join(sorted(set(aux)))
            else:
                temp[t_map[j]] = "-"
        if ind == 0:
            if "&" in v:
                k = ">*" + k
            else:
                k = ">" + k

        af[k] = temp

    af["*X"] = ["-" for _ in alph]

    hdr = ["-"]
    hdr.extend(alph)
    hdr = " ".join(hdr)

    return (hdr, af)


def af_union(af1, af2):
    """ Union of two AF's """
    try:
        src_af1 = tl.parser_afnd(af1[:-1])
        src_af2 = tl.parser_afnd(af2[:-1])
    except Exception:
        src_af1 = af1
        src_af2 = af2

    # Rename states
    clean_af1 = {}
    clean_af2 = {}

    i = '1'
    for state in src_af1:
        if state == '-' or state == '':
            continue
        clean_af1[state + i] = []
        for transitions_to in src_af1[state]:
            aux_tr = transitions_to.strip("*")+i if transitions_to != '-' else transitions_to
            clean_af1[state + i].append(aux_tr)

    i = '2'
    for state in src_af2:
        if state == '-' or state == '':
            continue
        clean_af2[state + i] = []
        for transitions_to in src_af2[state]:
            aux_tr = transitions_to.strip("*")+i if transitions_to != '-' else transitions_to
            clean_af2[state + i].append(aux_tr)
        
    # Set new header for the union AF
    alph_af1 = [x for x in src_af1['-']]
    alph_af2 = [x for x in src_af2['-']]
    hdr_union = sorted(list(set(alph_af1) | set(alph_af2)))
    header = ["-" , "&"]
    header.extend(hdr_union)
    hdr_union = header
    header = " ".join(header)

    # Find initial state for AF1 and AF2.
    q0_af1 = find_state(clean_af1, 'initial')[0]
    q0_af2 = find_state(clean_af2, 'initial')[0]

    # Find final states for AF1 and AF2.
    qf_af1 = find_state(clean_af1, 'final')
    qf_af2 = find_state(clean_af2, 'final')

    # Clean final states.
    for i, state in enumerate(qf_af1):
        qf_af1[i] = state.strip("*").strip(">")

    for i, state in enumerate(qf_af2):
        qf_af2[i] = state.strip("*").strip(">")

    # Create a new AF to be the union and set Q0.
    union_af = {}
    union_af[">Q0"] = [q0_af1.strip("*").strip(">") + ',' + q0_af2.strip("*").strip(">")]
    union_af[">Q0"].extend(['-' for _ in hdr_union[2:]])

    # Add the states to union AF.
    union_af =  union_helper(union_af, sanitize_af(clean_af1), hdr_union[2:], alph_af1)
    union_af = union_helper(union_af, sanitize_af(clean_af2), hdr_union[2:], alph_af2)

    # Add a &-transition to the new final state for the AF's final states.
    for state in union_af:
        if state in qf_af1 or state in qf_af2:
            union_af[state] = ['X'] + union_af[state][1:]

    # Create a new final state.
    union_af['*X'] = ["-" for _ in hdr_union[1:]]

    return (header, union_af)


def find_state(af, state):
    if state == 'initial':
        return [y for y in af.keys() if ">" in y]
    else:
        return [y for y in af.keys() if "*" in y]


def union_helper(union_af, af, header, af_symbols):
    for state in af:
        if state == '-':
            continue
        position = header.index(af_symbols[0]) + 1
        union_af[state] = (['-'] * position) + af[state]
        sizeof_null_transitions = (len(header) + 1) - len(union_af[state])
        union_af[state] = union_af[state] + (['-'] * sizeof_null_transitions)
    
    return union_af


def af_equiv(af):
    af_t = {}
    for k,v in af.items():
        if k == '-':
            continue
        af_t[k] = v
    af = af_t

    raw_start = [y for y in af.keys() if ">" in y]
    start = [y.strip('*').strip('>').strip('*') for y in raw_start]
    raw_accept = [y for y in af.keys() if "*" in y]
    accept = [y.strip('*').strip('>').strip('*') for y in raw_accept]
    afs = sanitize_af(af)
    states = list(afs.keys())
    indices = list(range(len(afs[states[0]])))

    sets = {} 
    sets[(0,)] = accept
    sets[(1,)] = [x for x in states if x not in accept]
    last_group = {}

    while last_group != sets:
        groups = {}
        for st in states:
            t_sets = [0] if st in accept else [1]
            for tr in afs[st]:
                for i, s in enumerate(item[1] for item in list(sets.items())):
                    if tr in s:
                        t_sets.append(i)
                        break
                else:
                    t_sets.append('-')
            try:
                groups[tuple(t_sets)].append(st)
            except KeyError:
                groups[tuple(t_sets)] = []
                groups[tuple(t_sets)].append(st)
        last_group, sets = sets, groups

    # Maps the new AFD
    s_map = {}
    new_s = ['X'+str(i) for i, _ in enumerate(list(sets.keys()))]
    for i, (_, v) in enumerate(sets.items()):
        for state in v:
            s_map[state] = new_s[i]

    new_accept = []
    for k, v in s_map.items():
        if k in start:
            new_start = v
        if k in accept:
            new_accept.append(v)
    
    almost = {}
    for k, v in afs.items():
        almost[s_map[k]] = []
        for state in v:
            almost[s_map[k]].append(s_map[state]) if state != '-' else almost[s_map[k]].append('-')

    # Mark initial/acceptance states
    final = {}
    for k, v in almost.items():
        temp = []
        if k in new_start:
            temp.append(">")
        if k in new_accept:
            temp.append("*")
        temp.append(k)
        final["".join(temp)] = almost[k]

    return final

    
def af_dead_states(af):
    raw_start = [y for y in af.keys() if ">" in y]
    raw_accept = [y for y in af.keys() if "*" in y]
    visited = []
    aux_visit = sanitize_af(af)
    to_visit = [x.strip('*').strip('>').strip('*') for x in raw_accept]
    
    while to_visit:
        visiting = to_visit.pop()
        if visiting not in visited:
            visited.append(visiting)
            for k, v in aux_visit.items():
                if any(x.find(visiting) > -1 for x in v):
                    to_visit.append(k) 

    temp = {}
    for k in visited:
        tmp_str = ''
        if any(x.find(k) > -1 for x in raw_accept):
            tmp_str += '*'
        if k in raw_start[0].strip('*').strip('>').strip('*'):
            tmp_str += '>'
        temp[tmp_str+k] = []
        for x in aux_visit[k]:
            temp[tmp_str+k].append(x if x in visited else '-')

    return temp
    

def af_unreachable(af):
    raw_start = [y for y in af.keys() if ">" in y]
    raw_accept = [y for y in af.keys() if "*" in y]
    visited = []
    aux_visit = sanitize_af(af)
    to_visit = [raw_start[0].strip('*').strip('>').strip('*')]

    while to_visit:
        visiting = to_visit.pop()
        if visiting not in visited:
            visited.append(visiting)
            to_visit.extend([x for x in aux_visit[visiting] if x not in visited and x != '-'])

    temp = {}
    for k in visited:
        tmp_str = ''
        if any(x.find(k) > -1 for x in raw_accept):
            tmp_str += '*'
        if k in raw_start[0].strip('*').strip('>').strip('*'):
            tmp_str += '>'
        temp[tmp_str+k] = aux_visit[k]

    return temp


def af_minimize(af, string=True):
    if not string:
        temp_af = ""
        for k,v in af.items():
            temp_af += "{} {}\n".format(k, " ".join(v))
    af = af if string else temp_af
   

    ind = af.index('\n')+1
    af_p = afnd_afd(af[:ind], af[ind:])

    af_reach = af_unreachable(af_p)
    af_dead = af_dead_states(af_reach)


    af_eq = af_equiv(af_dead)

    return af_reach, af_dead, af_eq


def af_union_m(af):
    try:
        ind1 = af[0][:-1].index('\n')
        ind2 = af[1][1:-1].index('\n')
        hdr1 = af[0][:-1][:ind1]
        hdr2 = af[1][1:-1][:ind2]
       
        t_map1 = {}
        for y, k in enumerate(hdr1.split(' ')[1:]):
            t_map1[y] = k
        t_map2 = {}
        for y, k in enumerate(hdr2.split(' ')[1:]):
            t_map2[y] = k
    
        af1s = tl.parser_afnd(af[0][:-1])
        af2s = tl.parser_afnd(af[1][1:-1])

    except Exception:
        hdr1 = af[0]['-']
        hdr2 = af[1]['-']
        t_map1 = {}
        for y, k in enumerate(hdr1):
            t_map1[y] = k
        t_map2 = {}
        for y, k in enumerate(hdr2):
            t_map2[y] = k
        af1s = af[0]
        af2s = af[1]

    ##### Dict below
    raw_start1 = [y for y in af1s.keys() if ">" in y]
    raw_start2 = [y for y in af2s.keys() if ">" in y]
    clean_start1 = [y.strip('*').strip('>').strip('*') for y in raw_start1]
    clean_start2 = [y.strip('*').strip('>').strip('*') for y in raw_start2]

    raw_accept1 = [y for y in af1s.keys() if "*" in y]
    raw_accept2 = [y for y in af2s.keys() if "*" in y]
    clean_accept1 = [y.strip('*').strip('>').strip('*') for y in raw_accept1]
    clean_accept2 = [y.strip('*').strip('>').strip('*') for y in raw_accept2]

    af1c = sanitize_af(af1s)
    af2c = sanitize_af(af2s)
    
    used_names = list(set(af1c)|set(af2c))
    # Rename AF1
    t_table1 = {}
    t_table2 = {}
    t_str = "X0"
    i = 0
    for k in af1c:
        if k == '-':
            continue
        while True:
            if t_str not in used_names:
                t_table1[k] = t_str
                used_names.append(t_str)
                i += 1
                t_str = "X" + str(i)
                break
            else:
                i += 1
                t_str = "X" + str(i)

    for k in af2c:
        if k == '-':
            continue
        while True:
            if t_str not in used_names:
                t_table2[k] = t_str
                used_names.append(t_str)
                i += 1
                t_str = "X" + str(i)
                break
            else:
                i += 1
                t_str = "X" + str(i)

    while True:
        if t_str not in used_names:
            used_names.append(t_str)
            initial = t_str
            i += 1
            t_str = "X" + str(i)
            break
        else:
            i += 1
            t_str = "X" + str(i)
    
    while True:
        if t_str not in used_names:
            used_names.append(t_str)
            end_state = t_str
            i += 1
            t_str = "X" + str(i)
            break
        else:
            i += 1
            t_str = "X" + str(i)

    new_hdr = ['-','&']
    t_set = set()
    for _, h in t_map1.items():
        t_set.add(h)
    for _, h in t_map2.items():
        t_set.add(h)

    new_hdr.extend(list(t_set)) 
    t_map12 = {}
    for y, k in enumerate(new_hdr[1:]):
        t_map12[k] = y

    final = {}
    # Fill union
    final[initial] = ['-' for x in t_map12]
    final[initial][t_map12['&']] = ','.join((t_table1[clean_start1[0]], t_table2[clean_start2[0]]))
    
    for k, v in af1c.items():
        if k == "-":
            continue
        for tr, st in enumerate(v):
            try:
                final[t_table1[k]][t_map12[t_map1[tr]]] = t_table1[st]
            except Exception:
                final[t_table1[k]] = ['-' for _ in t_map12]
                final[t_table1[k]][t_map12[t_map1[tr]]] = t_table1[st]

    for k, v in af2c.items():
        if k == "-":
            continue
        for tr, st in enumerate(v):
            try:
                final[t_table2[k]][t_map12[t_map2[tr]]] = t_table2[st]
            except Exception:
                final[t_table2[k]] = ['-' for _ in t_map12]
                final[t_table2[k]][t_map12[t_map2[tr]]] = t_table2[st]

    f_accept = [t_table1[x] for x in clean_accept1]
    f_accept2 = [t_table2[x] for x in clean_accept2]
    f_accept.extend(f_accept2)
    final2 = {}
    final2['>'+initial] = final[initial]
    final2['*'+end_state] = ['-' for _ in t_map12]
    for k, v in final.items():
        if k == initial:
            continue
        if k in f_accept:
            for j in v:
                final2[k]= [end_state] + v[1:]
        else:
            final2[k]= ['-'] + v[1:]
    
    return(" ".join(new_hdr) ,final2)


def af_intersection(af):
    af1 = tl.parser_afnd(af[0][:-1])
    af2 = tl.parser_afnd(af[1][1:-1])
    states = set(af1) | set(af2)

    af1_c = af_complement(af1, states)
    states = states | set(af1_c)
    af2_c = af_complement(af2, states)
    states = states | set(af2_c)

    af1_c['-'] = af1['-']
    af2_c['-'] = af2['-']
    u_hdr, af_u = af_union_m((af1_c, af2_c))
    states = states | set(af_u)

    temp_afu = ''
    for k, v in af_u.items():
        temp_afu += "{} {}\n".format(k, " ".join(v))
    t_hdr = ' '.join([x for x in u_hdr if x != ' ' and x != '\n']) + '\n'
    temp_afu = t_hdr + temp_afu
    
    af_min = af_minimize(temp_afu[:-1])[2]
    states = states|set(af_min)
    af_int = af_complement(af_min, states)

    return af1_c, af2_c, u_hdr, af_u, af_min, af_int


def af_complement(af, reserved):
    afp = sanitize_af(af)
    reserved_clean = [x.strip('*').strip('>').strip('*') for x in reserved if x != '-']
    temp = {}
    temp_k = 'X0'
    i = 0
    loop = True
    while loop:
        loop = False
        for k in list(afp.keys()):
            if k == '-':
                continue
            if temp_k in reserved_clean:
                i += 1
                temp_k = 'X' + str(i)
                loop = True
                break
    
    temp['*'+temp_k] = [temp_k for x in list(afp.items())[0][1]]

    for k, v in af.items():
        if '*' in k:
            st = list(k)
            st.remove('*')
            temp[''.join(st)] = ' '.join(v).replace('-', temp_k).split(' ')
        else:
            if k == '-':
                continue
            temp['*'+k] = ' '.join(v).replace('-', temp_k).split(' ')

    return temp


def chosmky_form(glc):
    glc = glc[:-1]

    # Construct a dict from GLC string.
    glc_dict = {}
    for line in glc.split("\n"):
        brk = line.split(" -> ")
        glc_dict[brk[0]] = brk[1:][0].split(' | ')

    # Create a S' production if S appears on right side.
    starting_symbol_on_right = remove_starting_symbol(glc[0], glc_dict)
    if starting_symbol_on_right is True:
        glc_dict[glc[0] + '`'] = [glc[0]]

    # Remove null productions.
    glc_dict = remove_null_productions(glc_dict)
    glc_without_nullprod = deepcopy(glc_dict)

    # Remove unitary productions.
    glc_without_unitprod = remove_unitary_productions(glc_dict)

    # Break productions with length > 2 and replace T for NT.
    glc_dict = transform_productions_chomsky(glc_without_unitprod)

    return glc_without_nullprod, glc_without_unitprod, glc_dict


def remove_starting_symbol(starting_symbol, glc):
    for symbol, production in glc.items():
        if any(x.find(starting_symbol) > -1 for x in production):
                return True
    
    return False


def remove_null_productions(glc):
    nullable_states = []
    direct_nullable = []

    # Find direct nullable states.
    for symbol, production in glc.items():
        if any(x.find('&') > -1 for x in production):
            direct_nullable.append(symbol)
            glc[symbol] = [x for x in glc[symbol] if x != '&']

    # Remove direct nullable states.
    for symbol, productions in glc.items():
        for prod in productions:
            for state in direct_nullable:
                if state in prod and len(prod) > 1:
                    glc[symbol].append(prod.replace(state, ""))
        
    # Find new nullable states.
    nullable_states = find_nullable_states(glc, direct_nullable)

    # Remove new nullable states.
    for symbol, productions in glc.items():
        for prod in productions:
            for state in nullable_states:
                if state in prod and len(prod) > 1:
                    for i in range(1, len(prod)):
                        new_prod = prod.replace(state, "", i)
                        glc[symbol].append(new_prod) if new_prod not in glc[symbol] else None
                        for letter in reversed(prod):
                            if letter == state and prod.count(letter) > 1:
                                new_value = prod.replace(letter, '', 1)[::-1]
                                glc[symbol].append(new_value) if new_value not in glc[symbol] else None

    return glc


def find_nullable_states(glc, nullable_states):
    for symbol, productions in glc.items():
        for prod in productions:
            for state in nullable_states:
                if prod == state and symbol not in nullable_states:
                    nullable_states.append(symbol)
                    nullable_states = find_nullable_states(glc, nullable_states)

    return nullable_states


def remove_unitary_productions(glc):
    heads = glc.keys()
    
    for head in heads:
        for symbol, productions in glc.items():
            for prod in productions:
                if head == prod:
                    index = glc[symbol].index(head)
                    del glc[symbol][index]
                    for production in glc[prod]:
                        glc[symbol].append(production) if production not in glc[symbol] else None
    return glc


def transform_productions_chomsky(glc):
   
    switched_productions = []
    new_productions = {}

    new_glc = deepcopy(glc)
    # Find all terminals and switch for new non-terminal.
    for symbol, productions in glc.items():
        for prod in productions:
            if len(prod) > 1:
                for letter in list(prod):
                    if (letter in ascii_lowercase or letter in list(map(lambda x: str(x), range(0, 10)))) and letter not in switched_productions:
                        new_symbol = choice([x for x in ascii_uppercase if x not in glc.keys()])
                        switched_productions.append(letter)
                        index = glc[symbol].index(prod)
                        old_value = new_glc[symbol][index]
                        new_glc[symbol][index] = old_value.replace(letter, new_symbol)
                        new_glc[new_symbol] = [letter]
                    elif (letter in ascii_lowercase or letter in list(map(lambda x: str(x), range(0, 10)))) and letter in switched_productions:
                        index = glc[symbol].index(prod)
                        old_value = new_glc[symbol][index]
                        new_glc[symbol][index] = old_value.replace(letter, new_symbol, 1)

    final_glc = deepcopy(new_glc)
    # Break more productions with length > 2;
    for symbol, productions in new_glc.items():
        for prod in productions:
            if len(prod) > 2 and prod not in new_productions.keys():
                new_symbol = choice([x for x in ascii_uppercase if x not in new_glc.keys()])
                new_glc[symbol].append(new_symbol + prod[2:])
                final_glc[new_symbol] = [prod[0:2]]
                new_productions[prod] = new_symbol
                index = final_glc[symbol].index(prod)
                old_value = final_glc[symbol][index]
                final_glc[symbol][index] = old_value.replace(prod[0:2], new_symbol)
            elif len(prod) > 2:
                new_symbol = new_productions[prod]
                index = final_glc[symbol].index(prod)
                old_value = final_glc[symbol][index]
                new_glc[symbol].append(new_symbol + prod[2:])
                final_glc[symbol][index] = old_value.replace(prod[0:2], new_symbol)

    return final_glc


def check_indirect_fact(glc, productions):
    indirect_prods = []

    for i, production in enumerate(productions):
        next_prod = i + 1 if i + 1 < len(productions) else i
        if production[0] in ascii_uppercase:
            for prod in glc[production[0]]:
                next_symbol = productions[next_prod][0]
                for prod_2 in glc[next_symbol]:
                    has_prod = False
                    max_range = len(prod) if len(prod) < len(prod_2) else len(prod_2)
                    for j in range(max_range):
                        if prod[j] == prod_2[j]:
                            has_prod = True
                            continue
                        elif has_prod:
                            indirect_prods.append(production[0])
                            indirect_prods.append(productions[next_prod][0])
                        else:
                            break
    
    return indirect_prods


def factorization(glc):
    glc = glc[:-1]

    # Construct a dict from GLC string.
    glc_dict = {}
    for line in glc.split("\n"):
        brk = line.split(" -> ")
        glc_dict[brk[0]] = brk[1:][0].split(' | ')

    glc_result = deepcopy(glc_dict)
    final_glc = deepcopy(glc_dict)

    for symbol, productions in glc_dict.items():
        if len(productions) > 1:
            same_indirect_prod = check_indirect_fact(glc_result, productions)
            if len(same_indirect_prod):
                glc_result = copy_new_symbols(symbol, glc_result, same_indirect_prod[0], same_indirect_prod[1])
            same_prod = check_direct_fact(glc_result[symbol])
            if len(same_prod) > 0: # Add new production in GLC.
                final_glc = remove_direct_fact(same_prod[0], symbol, glc_result[symbol], glc_result)

    return final_glc


def copy_new_symbols(target_symbol, glc, symbol1, symbol2):
    symbol1_prods = glc[symbol1]
    symbol2_prods = glc[symbol2]
    new_glc = deepcopy(glc)
    to_be_deleted = glc[target_symbol]

    for production in glc[target_symbol]:
        if production[0] == symbol1:
            for prod in symbol1_prods:
                new_prod = prod + production[1:]
                new_glc[target_symbol].append(new_prod)
        elif production[0] == symbol2:
            for prod in symbol2_prods:
                new_prod = prod + production[1:]
                new_glc[target_symbol].append(new_prod)

    new_glc[target_symbol] = list(filter(lambda x: x not in to_be_deleted, new_glc[target_symbol]))
    return new_glc


def remove_direct_fact(same_prod, current_state, productions, new_glc):
    prods = []
    changed_prods = []

    for production in productions:
        if same_prod in production:
            new_prod = production.replace(same_prod, "") if production.replace(same_prod, "") != "" else '&'
            prods.append(new_prod)
            changed_prods.append(production)
    new_symbol = choice([x for x in ascii_uppercase if x not in new_glc.keys()])
    new_glc[new_symbol] = prods
    new_glc[current_state].append(same_prod + new_symbol)
    new_glc[current_state] = list(filter(lambda x: x not in changed_prods, new_glc[current_state]))
    return new_glc


def check_direct_fact(productions):
    same_prod = []

    for i, production in enumerate(productions):
        next_prod = i + 1 if i + 1 < len(productions) else i
        for k in range(next_prod, len(productions)):
            max_range = len(productions[k]) if len(productions[k]) < len(production) else len(production)
            index = 0
            for j in range(max_range):
                if production != productions[k] and (production[j] == productions[k][j]):
                    index = j if j != 0 else 1
                else:
                    break
            same_prod.append(production[:index]) if production[:index] not in same_prod and production[:index] != "" else None

    return same_prod


    return same_prod    

def left_rec_new_states(key, glcd, states):
    # Find new state
    for new_s in ascii_uppercase:
        n_state = new_s if new_s not in states else None
        if n_state:
            break


    lineRm = []
    new_line = []
    for v in glcd[key].split(" | "):
        if v[0] == key:
            lineRm.append(''.join([x for x in v[1:]+n_state if x != "&"])) 
        else:
            new_line.append(v+n_state)

    lineRm2 = []
    for x in lineRm:
        if "&" in x:
            continue
        lineRm2.append(x)
    lineRm2.append("&")
    lineRm2.sort()
    new_line.sort()

    return new_line, lineRm2, n_state



def remove_left_rec_direct(glcd):
    states = list(glcd)

    temp = {}
    for k, v in glcd.items():
        tr = v.split(' | ')
        test = sum([1 for a in tr if a[0] == k])
        if test > 0:
            fix = left_rec_new_states(k,glcd,states)
            temp[k] = " | ".join(fix[0])
            temp[fix[2]] = " | ".join(fix[1])
            states.append(fix[2])
        else:
            temp[k] = v
    

    return temp

    
def remove_left_rec(glc):
    glcd = {}
    glc = glc[:-1]
    first = None
    for line in glc.split('\n'):
        st = line.split(" -> ")
        first = st[0] if not first else first
        glcd[st[0]] = st[1]

    temp_glc1 = remove_left_rec_direct(glcd)
    temp_glc1 = remove_left_rec_indirect(temp_glc1)
    temp_glc2 = glcd

    while temp_glc1 != temp_glc2:
        temp_glc_temp = remove_left_rec_direct(temp_glc1)
        temp_glc1, temp_glc2 = remove_left_rec_indirect(temp_glc_temp), temp_glc1
    
    return(temp_glc1, first)

def remove_left_rec_indirect(glcd):
    states = list(glcd)
    temp = glcd
    for k in states:
        tr = glcd[k].split(' | ')
        to_pass = [x[0] for x in tr if x[0] in ascii_uppercase and x[0] != k]
        for x in to_pass:
            if x == k:
                pass
            tt = []
            for tp in glcd[x].split(" | "):
                for st in tr:
                    if tp[0] == k:
                        tt.append(tp.replace(k, st, 1))
                    else:
                        tt.append(tp)
            temp[x] = " | ".join(sorted(list(set(tt))))
        
    return(temp)

def rec_ap(ap_raw, word):
    ap_s = states, i_alphabet, s_alphabet, transitions, start, stack, accept = ap_raw.split("|")

    # Create transitions dict
    transitions_d = {}
    for tr in transitions.split("!"):
        temp_tr = tr.split(",")
        key, value = ','.join(temp_tr[:3]), tuple(temp_tr[3:])
        transitions_d[key] = value

    current_state = start
    stack = list('$'+stack[::-1])
    for letter in word:
        try:
            tr = ','.join((current_state,letter,stack.pop()))
            next_state, next_stack = transitions_d[tr][0], transitions_d[tr][1][::-1]
            stack.append(next_stack) if next_stack else None
            current_state = next_state
        except Exception as err:
            return False

    if current_state in accept.split(",") and stack[-1] == '$':
        return True
    return False
