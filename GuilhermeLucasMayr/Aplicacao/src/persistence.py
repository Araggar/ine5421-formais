def save(path, str):
    # Savesi data to 'path'
    with open(path,'w') as file:
        file.write(str)


def load(path, *_):
    # Loads data from 'path'
    with open(path,'r') as file:
        temp =  file.read()
    return temp
