# INE5421-Formais

## Universidade Federal de Santa Catarina – UFSC
Departamento de Informática e Estatística - INE

Linguagens Formais e Compiladores - 05/05/2019

### Alunos
Guilherme Antônio Ferreira da Silva - 16100732

Lucas Mayr de Athayde - 16203113

## Requisitos
- Python na versão >= 3.0
- Pip, que é um sistema de gerênciamento de pacotes para Python.
- Tkinter, para a interface gráfica.

  >Observação: Por padrão, o Tkinter deveria acompanhar o Python e não seria necessário realizar nenhuma instalação do mesmo.

## Como utilizar
Com os requisitos instalados e na pasta raíz, é necessário executar o comando:

>`pip install -r requirements.txt`

Concluída a instalação dos pacotes necessários, o programa está pronto para ser executado. Basta executar o comando a seguir:

>`python main.py`

Dentro da interface, existem 3 menus:
- File
- AF(N)D
- GR

A esquerda, existe uma área de input que pode receber uma string em formato de ER, GR ou AFD.

### Específicando um AF:
Para específicar um AF, siga os passos:
1. A primeira linha deve ser o cabeçalho da tabela de transição.
2. As n próximas linhas devem ter o estado atual e suas transições, separados por espaço.
3. O estado inicial deve ter um '>' a sua esquerda.
4. Estados finais devem ter um '*' a sua esquerda.

>Exemplo:
>
>\- a b
>
>\>S A B
>
>*A S B
>
>*B A B

### Específicando mais de um AF para operações de união e intersecção:
Para utilizar a ferramenta da união ou intersecção, siga os passos:
1. Monte o primeiro AF da mesma maneira em que demonstrado anteriormente.
2. Adicione um '---' uma linha abaixo do AF.
3. Monte o segundo AF da mesma maneira que o primeiro e escolha a opção desejada.

>Exemplo:
>
>\- a
>
>\>S A
>
>*A S
>
>\---
>
>\- b
>
>\>S B
>
>*B B


### Específicando uma GR:
Para específicar uma GR, siga os passos:
1. Cada linha deve conter seu símbolo seguido de '->' e suas produções, separados por '|'.

> Exemplo: 
> 
> S -> aA | bA | b
> 
> A -> aS | a

### Usando a ferramenta
Tendo específicado, basta selecionar uma ação correspondente no menu.

A direita, após executar alguma ação, o resultado será mostrado.

O menu *File* permite salvar/carregar arquivos de texto que contenham a descrição de um ER/GR/AFD.

O menu *AF(N)D* inclui todas as operações possíveis de se executar com um AF, como determinizar, converter para uma gramática, entre outros.

O menu *GR* permite realizar a operação de conversão de gramática regular para um AFND.

### Testes
Para rodar os testes, é necessário estar dentro da pasta 'Testes' e executar o comando:

>`python testeDesejado.py`

Basta trocar *testeDesejado* pelo teste que inclui a ação a ser testada. No terminal serão mostrados os resultados para o teste executado.
Por exemplo, para rodar os testes de conversão de GR para AF, o seguinte comando seria executado:

>`python test4ConversaoGRtoAF.py`


## Linguagem e Bibliotecas
A linguagem utilizada foi Python, em sua versão 3. Para a interface gráfica, utilizamos a biblioteca PySimpleGui. Outros utilitários usados já estão inclusos na biblioteca standard da linguagem.

## Estruturas
Para representar a entrada e saída de autômatos, expressões regulares e gramáticas, utilizamos strings que são parseadas e então executamos as operações necessárias para obter o resultado, que é então convertido de volta em string.

Além disso, utilizamos dicionários para representar a estrutura durante as operações, por exemplo, uma gramática terá a forma <símbolo, produções>. 