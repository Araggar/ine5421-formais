import unittest
import sys

sys.path.append("../Aplicacao")
from src.formais import remove_left_rec as rec
from src.gui import GuiFormais

GLC_NONE = """S -> Ba
A -> aAS | a | &
B -> bS | A | bb
"""

GLC_DIRECT = """S -> Ba
A -> aAS | Aa | &
B -> bS | A | bb
"""

GLC_INDIRECT = """S -> aASA | AaB
A -> aS | a | Sa
B -> b | &
"""

GLC_BOTH = """S -> SaASA | AaB
A -> aS | a | Sa
B -> b | &
"""

draw = GuiFormais().draw_af


class TestGRtoAFMethods(unittest.TestCase):
    def test_rec_none(self):
        print("Teste NONE")
        print("GLC:\n{}".format(draw(GLC_NONE)))
        resp, first = rec(GLC_NONE)
        str_ = first+" -> "+resp[first]+"\n"
        for k,v in resp.items():
            if k == first:
                continue
            str_ += k+" -> "+v+"\n"
        print("NOVO GLC:\n{}".format(draw(str_)))

    def test_rec_direct(self):
        print("Teste Direct")
        print("GLC:\n{}".format(draw(GLC_DIRECT)))
        resp, first = rec(GLC_DIRECT)
        str_ = first+" -> "+resp[first]+"\n"
        for k,v in resp.items():
            if k == first:
                continue
            str_ += k+" -> "+v+"\n"
        print("NOVO GLC:\n{}".format(draw(str_)))

    def test_rec_indirect(self):
        print("Teste Indirect")
        print("GLC:\n{}".format(draw(GLC_INDIRECT)))
        resp, first = rec(GLC_INDIRECT)
        str_ = first+" -> "+resp[first]+"\n"
        for k,v in resp.items():
            if k == first:
                continue
            str_ += k+" -> "+v+"\n"
        print("NOVO GLC:\n{}".format(draw(str_)))
    
    def test_rec_both(self):
        print("Teste Indirect and Direct")
        print("GLC:\n{}".format(draw(GLC_BOTH)))
        resp, first = rec(GLC_BOTH)
        str_ = first+" -> "+resp[first]+"\n"
        for k,v in resp.items():
            if k == first:
                continue
            str_ += k+" -> "+v+"\n"
        print("NOVO GLC:\n{}".format(draw(str_)))

if __name__ == "__main__":
    unittest.main()
