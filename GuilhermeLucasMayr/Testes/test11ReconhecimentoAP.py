import unittest
import sys

sys.path.append("../Aplicacao")
from src.formais import rec_ap as rec
from src.gui import GuiFormais

AP_ACCEPT_SIMPLE = """ab
A0,B1,B2|a,b|Q,P|A0,a,Q,B1,P!B1,b,P,B1,!B1,&,P,B1,|A0|Q|B1,B2"""
AP_ACCEPT = """bbbbbab
A0,B1,B2|a,b|Q,P|A0,b,Q,A0,Q!A0,a,Q,B1,P!B1,b,P,B1,!B1,a,P,B2,!B1,&,P,B1,|A0|Q|B1"""
AP_REJECT = """aabb
A0,B1,B2|a,b|Q,P|A0,b,Q,A0,Q!A0,a,Q,B1,P!B1,b,P,B1,!B1,a,P,B2,!B1,&,P,B1,|A0|Q|B1"""
AP_NO_TRANSITION = """aabaa
A0,B1,B2|a,b|Q,P|A0,b,Q,A0,Q!A0,a,Q,B1,P!B1,b,P,B1,!B1,a,P,B2,!B1,&,P,B1,|A0|Q|B1"""
AP_WRONG_LETTER = """abc
A0,B1,B2|a,b|Q,P|A0,a,Q,B1,P!B1,b,P,B1,!B1,&,P,B1,|A0|Q|B1,B2"""


def format_ap(ap_raw):
    ap_s = states, i_alphabet, s_alphabet, transitions, start, stack, accept = ap_raw.split("|")
    tr_str = ''
    for tr in transitions.split("!"):
        tr_str += '('+tr+'),'
    tr_str = tr_str[:-1]

    str_ = '('+states+'),'+'('+i_alphabet+'),'+'('+s_alphabet+'),'+'('+tr_str+'),'+'('+start+'),'+'('+stack+'),'+'('+accept+')'
    return str_



class TestGRtoAFMethods(unittest.TestCase):
    def test_rec_accept_simple(self):
        print("Teste aceitação")
        wrd, ap = AP_ACCEPT_SIMPLE.split('\n')
        print("Input: ", wrd)
        print("AP: ", format_ap(ap))
        resp = rec(ap, wrd)
        print("Aceita? : ", resp)

    def test_rec_accept(self):
        print("Teste aceitação")
        wrd, ap = AP_ACCEPT.split('\n')
        print("Input: ", wrd)
        print("AP: ", format_ap(ap))
        resp = rec(ap, wrd)
        print("Aceita? : ", resp)

    def test_rec_reject(self):
        print("\nTeste estado de rejeição")
        wrd, ap = AP_REJECT.split('\n')
        print("Input: ", wrd)
        print("AP: ", format_ap(ap))
        resp = rec(ap, wrd)
        print("Aceita? : ", resp)
    
    def test_rec_no_tr(self):
        print("\nTeste transição inválida")
        wrd, ap = AP_NO_TRANSITION.split('\n')
        print("Input: ", wrd)
        print("AP: ", format_ap(ap))
        resp = rec(ap, wrd)
        print("Aceita? : ", resp)
    
    def test_rec_wrong(self):
        print("\nTeste letra fora do alfabeto")
        wrd, ap = AP_WRONG_LETTER.split('\n')
        print("Input: ", wrd)
        print("AP: ", format_ap(ap))
        resp = rec(ap, wrd)
        print("Aceita? : ", resp)

if __name__ == "__main__":
    unittest.main()
