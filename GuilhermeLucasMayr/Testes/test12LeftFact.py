import unittest
import sys

sys.path.append("../Aplicacao")
from src.formais import factorization
from src.gui import GuiFormais

draw = GuiFormais().draw_af

GLC_NONE  = """S -> Ba
A -> b | aa
B -> a | & | Cb
C -> d | e
"""

GLC_DIRECT = """S -> aA | aB
B -> b | c
C -> dA | fB | dD
D -> f
"""

GLC_INDIRECT = """S -> AC | BC
A -> aD | cC
B -> aB | dD
C -> eC | eA
D -> fD | CB
"""

GLC_BOTH = """S -> aAd | aB
A -> a | ab
B -> ccd | ddc
"""

class TestFactMethods(unittest.TestCase):
    def test_fact_1(self):
        glc_result = factorization(GLC_NONE)
        result = ""
        for k, v in glc_result.items():
            result += "{} -> {}\n".format(k, " | ".join(v))
        print("GLC Fatorada:\n{}".format(draw(result)))

    def test_fact_2(self):
        glc_result = factorization(GLC_DIRECT)
        result = ""
        for k, v in glc_result.items():
            result += "{} -> {}\n".format(k, " | ".join(v))
        print("GLC Fatorada:\n{}".format(draw(result)))

    def test_fact_3(self):
        glc_result = factorization(GLC_INDIRECT)
        result = ""
        for k, v in glc_result.items():
            result += "{} -> {}\n".format(k, " | ".join(v))
        print("GLC Fatorada:\n{}".format(draw(result)))
    
    def test_fact_5(self):
        glc_result = factorization(GLC_BOTH)
        result = ""
        for k, v in glc_result.items():
            result += "{} -> {}\n".format(k, " | ".join(v))
        print("GLC Fatorada:\n{}".format(draw(result)))


if __name__ == "__main__":
    unittest.main()