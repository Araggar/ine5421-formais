import unittest
import sys


sys.path.append('../Aplicacao')
from src.formais import af_handler
from src.gui import GuiFormais

def dict_af(d):
    naf = ''
    for k, v in d.items():
        naf += "{} {}\n".format(k, ' '.join(v))
    return naf

draw = GuiFormais().draw_af

acc_str = "aaabaaccaacccabca"

rej_str = "cbaaaa"

empty_str = ""

inv_str = "aaadbaabbaba"

inv_trans = "aaabbaac"

af_rej_empty = """- & a b c
>Q0 - Q0,Q1 Q1 Q2
Q1 - Q1 - Q0
*Q2 Q1 Q1 Q1 -"""

af = """- & a b c
>*Q0 Q2 Q0,Q1 Q2 Q2
Q1 - Q1 - Q0
*Q2 Q1 Q1 Q1 -"""

class TestReconhecimentoAfd(unittest.TestCase):
    """ Tipos -> AFD, AFND, AFND&
Conversoes Possiveis:
    AFD, AFD
    AFND, AFD
    AFND&, AFD """

    def test_accept(self):
        ind = af.index('\n') + 1
        hdr = af[:ind]
        src = af[ind:]
        afn, result, last, path, err = af_handler(acc_str,hdr,src)
        print("AF original:\n\n{}AF resultante:\n{}\nReconhece? - {}\nParou no estado - {}\nCaminho percorrido - {}\nErrors - {}\n".format(draw(af),draw(dict_af(afn)), result, last, ' -> '.join(path),err))
        self.assertEqual(result, True)

    def test_reject(self):
        ind = af.index('\n') + 1
        hdr = af[:ind]
        src = af[ind:]
        afn, result, last, path, err = af_handler(rej_str,hdr,src)
        print("AF original:\n\n{}AF resultante:\n{}\nReconhece? - {}\nParou no estado - {}\nCaminho percorrido - {}\nErrors - {}\n".format(draw(af),draw(dict_af(afn)), result, last, ' -> '.join(path),err))
        self.assertEqual(result, False)

    def test_empty_accept(self):
        ind = af.index('\n') + 1
        hdr = af[:ind]
        src = af[ind:]
        afn, result, last, path, err = af_handler(empty_str,hdr,src)
        print("AF original:\n\n{}AF resultante:\n{}\nReconhece? - {}\nParou no estado - {}\nCaminho percorrido - {}\nErrors - {}\n".format(draw(af),draw(dict_af(afn)), result, last, ' -> '.join(path),err))
        self.assertEqual(result, True)
         
    def test_empty_reject(self):
        ind = af_rej_empty.index('\n') + 1
        hdr = af_rej_empty[:ind]
        src = af_rej_empty[ind:]
        afn, result, last, path, err = af_handler(empty_str,hdr,src)
        print("AF original:\n\n{}AF resultante:\n{}\nReconhece? - {}\nParou no estado - {}\nCaminho percorrido - {}\nErrors - {}\n".format(draw(af_rej_empty),draw(dict_af(afn)), result, last, ' -> '.join(path),err))
        self.assertEqual(result, False)

    def test_invalid_transition(self):
        ind = af.index('\n') + 1
        hdr = af[:ind]
        src = af[ind:]
        afn, result, last, path, err = af_handler(inv_trans,hdr,src)
        print("AF original:\n\n{}AF resultante:\n{}\nReconhece? - {}\nParou no estado - {}\nCaminho percorrido - {}\nErrors - {}\n".format(draw(af),draw(dict_af(afn)), result, last, ' -> '.join(path),err))
        self.assertEqual(result, False)

    def test_invalid_token(self):
        ind = af.index('\n') + 1
        hdr = af[:ind]
        src = af[ind:]
        afn, result, last, path, err = af_handler(inv_str,hdr,src)
        print("AF original:\n\n{}AF resultante:\n{}\nReconhece? - {}\nParou no estado - {}\nCaminho percorrido - {}\nErrors - {}\n".format(draw(af),draw(dict_af(afn)), result, last, ' -> '.join(path),err))
        self.assertEqual(result, False)


if __name__ == "__main__":
    unittest.main()
