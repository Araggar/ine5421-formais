import unittest
import sys

sys.path.append("../Aplicacao")
from src.formais import gr_afnd
from src.gui import GuiFormais

gr1 = """S -> aA | bB | b
A -> aS
B -> bB | b
"""

gr2 = """S -> aA | bB | b | &
A -> aS
B -> bB | b
"""

gr3 = """S' -> & | S
S -> aA | bB | c
A -> aS | bS
B -> aC | bC
C -> c | cS
"""

gr4 = """S -> 0A | 1B | 0 | 1
A -> 0C | 1B | 0 | 1
B -> 0A | 1D | 0 | 1
C -> 1B | 1
D -> 0A | 0
"""

gr5 = """S -> aA | bS | a
A -> aB | bA | b
B -> aC | bB
C -> aD | bC
D -> aE | bD | a
E -> aS | bE | b
"""

draw = GuiFormais().draw_af


class TestGRtoAFMethods(unittest.TestCase):
    def test_gr_to_af_1(self):
        print("GR:\n{}".format(draw(gr1)))
        header, af = gr_afnd(gr1)
        result = ""
        for k, v in af.items():
            result += "{} {}\n".format(k, " ".join(v))
        result = header + "\n" + result
        print("AF:\n{}".format(draw(result)))

    def test_gr_to_af_2(self):
        print("GR:\n{}".format(draw(gr2)))
        header, af = gr_afnd(gr2)
        result = ""
        for k, v in af.items():
            result += "{} {}\n".format(k, " ".join(v))
        result = header + "\n" + result
        print("AF:\n{}".format(draw(result)))

    def test_gr_to_af_3(self):
        print("GR:\n{}".format(draw(gr3)))
        header, af = gr_afnd(gr3)
        result = ""
        for k, v in af.items():
            result += "{} {}\n".format(k, " ".join(v))
        result = header + "\n" + result
        print("AF:\n{}".format(draw(result)))

    def test_gr_to_af_4(self):
        print("GR:\n{}".format(draw(gr4)))
        header, af = gr_afnd(gr4)
        result = ""
        for k, v in af.items():
            result += "{} {}\n".format(k, " ".join(v))
        result = header + "\n" + result
        print("AF:\n{}".format(draw(result)))

    def test_gr_to_af_5(self):
        print("GR:\n{}".format(draw(gr5)))
        header, af = gr_afnd(gr5)
        result = ""
        for k, v in af.items():
            result += "{} {}\n".format(k, " ".join(v))
        result = header + "\n" + result
        print("AF:\n{}".format(draw(result)))


if __name__ == "__main__":
    unittest.main()
