import unittest
import sys

sys.path.append("../Aplicacao")
from src.formais import afd_gr
from src.gui import GuiFormais

draw = GuiFormais().draw_af

af1 = """- a b
>S A B
*A S C
B C S
C B A
"""
af2 = """- a b
*>S A B
A S C
B C S
*C A B
"""
af3 = """- a b c
>S A B C 
A B C A
B C A B
C D D D
D A B E
*E S S S
"""

af4 = """- a b c 
>Q0 Q0 X1 X0
*X0 X0 X0 X0
X1 X1 X0 X0
"""


class TestAFtoGRMethods(unittest.TestCase):
    def test_af_to_gr_1(self):
        print("AF:\n{}".format(draw(af1)))
        gr1 = afd_gr(af1)
        print("GR:\n{}".format(draw(gr1)))

    def test_af_to_gr_2(self):
        print("AF:\n{}".format(draw(af2)))
        gr2 = afd_gr(af2)
        print("GR:\n{}".format(draw(gr2)))

    def test_af_to_gr_3(self):
        print("AF:\n{}".format(draw(af3)))
        gr3 = afd_gr(af3)
        print("GR:\n{}".format(draw(gr3)))

    def test_af_to_gr_4(self):
        print("AF:\n{}".format(draw(af4)))
        gr4 = afd_gr(af4)
        print("GR:\n{}".format(draw(gr4)))


if __name__ == "__main__":
    unittest.main()
