import unittest
import sys

sys.path.append("../Aplicacao")
from src.formais import af_union
from src.gui import GuiFormais

draw = GuiFormais().draw_af

AF1 = """
- a b
>Q1 Q3 Q4
Q2 Q2 Q3
*Q3 Q2 Q3
Q4 Q4 Q4
"""

AF2 = """
- a b
>Q5 Q8 Q6
Q6 Q7 Q6
*Q7 Q7 Q6
Q8 Q8 Q8
"""

AF3 = """
- a b
>Q0 Q2 Q3
Q1 Q1 Q2
*Q2 Q1 Q2
Q3 Q3 Q3
"""

AF4 = """
- a b
>Q0 Q4 Q1
Q1 Q3 Q1
*Q3 Q3 Q1
Q4 Q4 Q4
"""

AF5 = """- a b
>S A -
A A -
*B - -
"""

AF6 = """- a b
*>A - B
B - C
*C - -
"""

class TestUnionAFMethods(unittest.TestCase):
    def test_union_1(self):
        print("AF 1:\n{}".format(draw(AF1)))
        print("AF 2:\n{}".format(draw(AF2)))
        hdr, union_result = af_union(AF1, AF2)

        final_union = ""
        for k, v in union_result.items():
            final_union += "{} {}\n".format(k, " ".join(v))
        final_union = hdr + "\n" + final_union

        print("Uniao:\n{}".format(draw(final_union)))

    def test_union_2(self):
        print("AF 1:\n{}".format(draw(AF3)))
        print("AF 2:\n{}".format(draw(AF4)))
        hdr, union_result = af_union(AF3, AF4)

        final_union = ""
        for k, v in union_result.items():
            final_union += "{} {}\n".format(k, " ".join(v))
        final_union = hdr + "\n" + final_union

        print("Uniao:\n{}".format(draw(final_union)))

    def test_union_3(self):
        print("AF 1:\n{}".format(draw(AF5)))
        print("AF 2:\n{}".format(draw(AF6)))
        hdr, union_result = af_union(AF5, AF6)

        final_union = ""
        for k, v in union_result.items():
            final_union += "{} {}\n".format(k, " ".join(v))
        final_union = hdr + "\n" + final_union

        print("Uniao:\n{}".format(draw(final_union)))


if __name__ == "__main__":
    unittest.main()
