import unittest
import sys

sys.path.append("../Aplicacao")
from src.formais import af_minimize
from src.gui import GuiFormais

draw = GuiFormais().draw_af

AF1 = "- 0 1\n>A B C\nB A D\n*C E F\n*D E F\n*E E F\nF F F"

AF2 = "- 0 1\n>*A G B\nB F E\nC C G\n*D A H\nE E A\nF B C\n*G G F\nH H D"

AF3 = "- a b\n>Q0 Q1 Q2\nQ1 Q1 Q3\nQ2 Q1 Q2\nQ3 Q1 Q4\n*Q4 Q1 Q2"

class TestMinimizationMethods(unittest.TestCase):
    def test_minimization_1(self):
        print("AF original:\n{}".format(draw(AF1)))
        af_alcanc, af_mortos, af_equiv = af_minimize(AF1)
        
        final_alc = ""
        for k, v in af_alcanc.items():
            final_alc += "{} {}\n".format(k, " ".join(v))
        
        final_mortos = ""
        for k, v in af_mortos.items():
            final_mortos += "{} {}\n".format(k, " ".join(v))

        final_equiv = ""
        for k, v in af_equiv.items():
            final_equiv += "{} {}\n".format(k, " ".join(v))

        print("AF sem estados inalcançaveis:\n{}".format(draw(final_alc)))
        print("AF sem estados mortos:\n{}".format(draw(final_mortos)))
        print("AF minimizado:\n{}".format(draw(final_equiv)))

    def test_minimization_2(self):
        print("AF original:\n{}".format(draw(AF2)))
        af_alcanc, af_mortos, af_equiv = af_minimize(AF2)
        
        final_alc = ""
        for k, v in af_alcanc.items():
            final_alc += "{} {}\n".format(k, " ".join(v))
        
        final_mortos = ""
        for k, v in af_mortos.items():
            final_mortos += "{} {}\n".format(k, " ".join(v))

        final_equiv = ""
        for k, v in af_equiv.items():
            final_equiv += "{} {}\n".format(k, " ".join(v))

        print("AF sem estados inalcançaveis:\n{}".format(draw(final_alc)))
        print("AF sem estados mortos:\n{}".format(draw(final_mortos)))
        print("AF minimizado:\n{}".format(draw(final_equiv)))


    def test_minimization_3(self):
        print("AF original:\n{}".format(draw(AF3)))
        af_alcanc, af_mortos, af_equiv = af_minimize(AF3)
        
        final_alc = ""
        for k, v in af_alcanc.items():
            final_alc += "{} {}\n".format(k, " ".join(v))
        
        final_mortos = ""
        for k, v in af_mortos.items():
            final_mortos += "{} {}\n".format(k, " ".join(v))

        final_equiv = ""
        for k, v in af_equiv.items():
            final_equiv += "{} {}\n".format(k, " ".join(v))

        print("AF sem estados inalcançaveis:\n{}".format(draw(final_alc)))
        print("AF sem estados mortos:\n{}".format(draw(final_mortos)))
        print("AF minimizado:\n{}".format(draw(final_equiv)))
if __name__ == "__main__":
    unittest.main()