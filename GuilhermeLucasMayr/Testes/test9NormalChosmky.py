import unittest
import sys

sys.path.append("../Aplicacao")
from src.formais import chosmky_form
from src.gui import GuiFormais

draw = GuiFormais().draw_af

GLC_1 = """S -> ASA | aB
A -> B | S
B -> b | &
"""

GLC_2 = """S -> ASB
A -> aAS | a | &
B -> SbS | A | bb
"""

GLC_3 = """S -> aASA | AaB
A -> aS | a | Sa
B -> b | &
"""

GLC_4 = """S -> 1ASA | A1B
A -> 1S | 1 | S1
B -> 0 | &
"""

class TestNormalChomskyMethods(unittest.TestCase):
    def test_chosmky_1(self):
        print("GLC:\n{}".format(draw(GLC_1)))
        glc_without_nullprod, glc_without_unitprod, glc_chomsky = chosmky_form(GLC_1)
        result_nullprod = ""
        for k, v in glc_without_nullprod.items():
            result_nullprod += "{} -> {}\n".format(k, " | ".join(v))
        print("GLC sem produções nulas:\n{}".format(draw(result_nullprod)))
        result_unitprod = ""
        for k, v in glc_without_unitprod.items():
            result_unitprod += "{} -> {}\n".format(k, " | ".join(v))
        print("GLC sem produções unitarias:\n{}".format(draw(result_unitprod)))
        result = ""
        for k, v in glc_chomsky.items():
            result += "{} -> {}\n".format(k, " | ".join(v))
        print("GLC na Forma Normal de Chomsky:\n{}".format(draw(result)))

    def test_chosmky_2(self):
        print("GLC:\n{}".format(draw(GLC_2)))
        glc_without_nullprod, glc_without_unitprod, glc_chomsky = chosmky_form(GLC_2)
        result_nullprod = ""
        for k, v in glc_without_nullprod.items():
            result_nullprod += "{} -> {}\n".format(k, " | ".join(v))
        print("GLC sem produções nulas:\n{}".format(draw(result_nullprod)))
        result_unitprod = ""
        for k, v in glc_without_unitprod.items():
            result_unitprod += "{} -> {}\n".format(k, " | ".join(v))
        print("GLC sem produções unitarias:\n{}".format(draw(result_unitprod)))
        result = ""
        for k, v in glc_chomsky.items():
            result += "{} -> {}\n".format(k, " | ".join(v))
        print("GLC na Forma Normal de Chosmky:\n{}".format(draw(result)))

    def test_chosmky_3(self):
        print("GLC:\n{}".format(draw(GLC_3)))
        glc_without_nullprod, glc_without_unitprod, glc_chomsky = chosmky_form(GLC_3)
        result_nullprod = ""
        for k, v in glc_without_nullprod.items():
            result_nullprod += "{} -> {}\n".format(k, " | ".join(v))
        print("GLC sem produções nulas:\n{}".format(draw(result_nullprod)))
        result_unitprod = ""
        for k, v in glc_without_unitprod.items():
            result_unitprod += "{} -> {}\n".format(k, " | ".join(v))
        print("GLC sem produções unitarias:\n{}".format(draw(result_unitprod)))
        result = ""
        for k, v in glc_chomsky.items():
            result += "{} -> {}\n".format(k, " | ".join(v))
        print("GLC na Forma Normal de Chosmky:\n{}".format(draw(result)))

    def test_chosmky_4(self):
        print("GLC:\n{}".format(draw(GLC_4)))
        glc_without_nullprod, glc_without_unitprod, glc_chomsky = chosmky_form(GLC_4)
        result_nullprod = ""
        for k, v in glc_without_nullprod.items():
            result_nullprod += "{} -> {}\n".format(k, " | ".join(v))
        print("GLC sem produções nulas:\n{}".format(draw(result_nullprod)))
        result_unitprod = ""
        for k, v in glc_without_unitprod.items():
            result_unitprod += "{} -> {}\n".format(k, " | ".join(v))
        print("GLC sem produções unitarias:\n{}".format(draw(result_unitprod)))
        result = ""
        for k, v in glc_chomsky.items():
            result += "{} -> {}\n".format(k, " | ".join(v))
        print("GLC na Forma Normal de Chosmky:\n{}".format(draw(result)))
    

if __name__ == "__main__":
    unittest.main()