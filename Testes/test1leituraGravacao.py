import unittest
import sys


sys.path.append('../Aplicacao')
from src.persistence import save, load



af = """aaaaaaa
- & a b c
>*A C AB B C
B - B - A
*C B B B -"""

gr = """
S` -> & A 
A -> aAB bB c cC 
B -> aB b c cA 
C -> &B aB bB c
"""
er = """
(((ab)*)|((ba)+))*
"""

class TestSaveLoadMethods(unittest.TestCase):

    def test_saveload_af(self):
        path = './testSaveAF.txt'
        save(path, af)
        print('Dados salvos:\n{}'.format(af))
        loaded = load(path)
        print('Dados carregados:\n{}\n'.format(loaded))
        self.assertEqual(loaded, af)

    def test_saveload_gr(self):
        path = './testSaveGR.txt'
        save(path, gr)
        print('Dados salvos:{}'.format(gr))
        loaded = load(path)
        print('Dados carregados:{}'.format(loaded))
        self.assertEqual(loaded, gr)

    def test_saveload_er(self):
        path = './testSaveER.txt'
        save(path, er)
        print('Dados salvos:{}'.format(er))
        loaded = load(path)
        print('Dados carregados:{}'.format(loaded))
        self.assertEqual(loaded, er)

if __name__ == '__main__':
    unittest.main()
