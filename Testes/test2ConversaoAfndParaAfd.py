import unittest
import sys


sys.path.append('../Aplicacao')
from src.formais import afnd_afd
from src.gui import GuiFormais

draw = GuiFormais().draw_af

afnde = """- & a b c
>Q0 - Q0 Q1 Q2 
Q1 Q0 Q1 Q2 -
*Q2 Q1 Q2 - Q0"""

afd = """- a b c d
>*Q0 Q2 Q0 Q1 Q2
Q1 - Q1 - Q0
*Q2 Q1 Q1 Q1 -"""

afnd = """- a b c d
>*Q0 Q2 Q0,Q1 Q1 Q2
Q1 - Q1 - Q0
*Q2 Q1 Q1 Q1 -"""

afnde_2 = """- & a b c
>*Q0 Q2 Q1,Q2 Q1 Q2
Q1 - Q1 - Q0
*Q2 Q1 Q1 Q1 -"""

def dict_af(d):
    naf = ''
    for k, v in d.items():
        naf += "{} {}\n".format(k, ' '.join(v))
    return naf

class TestAfndToAfdMethods(unittest.TestCase):
    """ Tipos -> AFD, AFND, AFND&
Conversoes Possiveis:
    AFD, AFD
    AFND, AFD
    AFND&, AFD """

    def test_AFD_AFD(self):
        print("Automato original:\n{}".format(draw(afd)))
        ind = afd.index('\n') + 1
        hdr = afd[:ind]
        src = afd[ind:]
        afd_a = afnd_afd(hdr,src)
        print("Automato determinizado (Mantendo colunas):\n{}".format(draw(dict_af(afd_a))))
   
    def test_AFND_AFD(self):
        print("Automato original:\n{}".format(draw(afnd)))
        ind = afnd.index('\n') + 1
        hdr = afnd[:ind]
        src = afnd[ind:]
        afd_a = afnd_afd(hdr,src)
        print("Automato determinizado (Mantendo colunas):\n{}".format(draw(dict_af(afd_a))))

    def test_AFNDe_AFD(self):
        print("Automato original:\n{}".format(draw(afnde)))
        ind = afnde.index('\n') + 1
        hdr = afnde[:ind]
        src = afnde[ind:]
        afd_a = afnd_afd(hdr,src)
        print("Automato determinizado (Mantendo colunas):\n{}".format(draw(dict_af(afd_a))))
         
    def test_AFNDe2_AFD(self):
        print("Automato original:\n{}".format(draw(afnde_2)))
        ind = afnde_2.index('\n') + 1
        hdr = afnde_2[:ind]
        src = afnde_2[ind:]
        afd_a = afnd_afd(hdr,src)
        print("Automato determinizado (Mantendo colunas):\n{}".format(draw(dict_af(afd_a))))


if __name__ == "__main__":
    unittest.main()
