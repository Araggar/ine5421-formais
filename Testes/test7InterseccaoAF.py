import unittest
import sys

sys.path.append("../Aplicacao")
from src.formais import af_intersection
from src.gui import GuiFormais

draw = GuiFormais().draw_af

AF_ALL = """- a b c
>Q0 Q0 X1 X0
*X0 X0 X0 X0
X1 X3 X0 X2
X3 X1 X0 -
X2 X2 X2 X2
X4 X4 X4 X4
---
- a b c
>Q0 Q0 X1 X0
*X0 X0 X0 X0
X1 X3 X0 X2
X3 X1 X0 -
X2 X2 X2 X2
X4 X4 X4 X4
"""

AF_NONE = """- a b c
>Q0 Q0 X1 X0
*X0 X0 X0 X0
X1 X3 X0 X2
X3 X1 X0 -
X2 X2 X2 X2
X4 X4 X4 X4
---
- a b c
>Q00 Q00 X10 X00
*X00 X00 X00 X00
X10 X30 X00 X20
X30 X10 X00 -
X20 X20 X20 X20
X40 X40 X40 X40
"""

AF_SOME = """- a b c
>Q0 Q0 X1 X0
*X0 X0 X0 X0
X1 X3 X0 X2
X3 X1 X0 -
X2 X2 X2 X2
X4 X4 X4 X4
---
- a b c
>Q0 Q0 X1 -
*X0 X0 X0 X0
X1 X3 X0 X2
X3 X1 X0 -
X2 X1 X2 X2
X4 X4 X3 X4
"""

class TestIntesectionAFMethods(unittest.TestCase):
    def test_intersection_all(self):
        print('Same Test\n')
        fnt = AF_ALL.split('---')
        af1_c, af2_c, u_hdr, af_u, af_min, af_int = af_intersection(AF_ALL.split('---'))
        u_hdr += '\n'
        ind1 = fnt[0].index("\n") + 1
        ind2 = fnt[1][1:].index("\n") + 1
        af1_c_str = ""

        for k, v in af1_c.items():
            af1_c_str += "{} {}\n".format(k, " ".join(v))
        fhdr = " ".join([x for x in fnt[0][:ind1] if x != "&" and x != " " and x != "\n"]) + "\n"
        af1_c_str = fhdr + af1_c_str

        af2_c_str = ""
        for k, v in af2_c.items():
            af2_c_str += "{} {}\n".format(k, " ".join(v))
        fhdr = " ".join([x for x in fnt[1][:ind2] if x != "&" and x != " " and x != "\n"]) + "\n"
        af2_c_str = fhdr + af2_c_str

        af_u_str = ""
        for k, v in af_u.items():
            af_u_str += "{} {}\n".format(k, " ".join(v))
        fhdr = " ".join([x for x in fnt[0][:ind1] if x != "&" and x != " " and x != "\n"]) + "\n"
        af_u_str = u_hdr + af_u_str

        af_min_str = ""
        for k, v in af_min.items():
            af_min_str += "{} {}\n".format(k, " ".join(v))
        fhdr = " ".join([x for x in u_hdr if x != "&" and x != " " and x != "\n"]) + "\n"
        af_min_str = fhdr + af_min_str

        af_int_str = ""
        for k, v in af_int.items():
            af_int_str += "{} {}\n".format(k, " ".join(v))
        fhdr = " ".join([x for x in u_hdr if x != "&" and x != " " and x != "\n"]) + "\n"
        af_int_str = fhdr + af_int_str
        
        f_str = "AF1:\n{}\nAF2:\n{}\nComplement AF1:\n{}\nComplement AF2\n{}\nAF1&2 Union:\n{}\nMinimized AF:\n{}\nIntersection:\n{}\n".format(draw(fnt[0]),draw(fnt[1]),draw(af1_c_str),draw(af2_c_str),draw(af_u_str),draw(af_min_str),draw(af_int_str))
        print(f_str)

    def test_intersection_none(self):
        print('\nNone test\n')
        fnt = AF_NONE.split('---')
        af1_c, af2_c, u_hdr, af_u, af_min, af_int = af_intersection(AF_NONE.split('---'))
        u_hdr += '\n'
        ind1 = fnt[0].index("\n") + 1
        ind2 = fnt[1][1:].index("\n") + 1
        af1_c_str = ""

        for k, v in af1_c.items():
            af1_c_str += "{} {}\n".format(k, " ".join(v))
        fhdr = " ".join([x for x in fnt[0][:ind1] if x != "&" and x != " " and x != "\n"]) + "\n"
        af1_c_str = fhdr + af1_c_str

        af2_c_str = ""
        for k, v in af2_c.items():
            af2_c_str += "{} {}\n".format(k, " ".join(v))
        fhdr = " ".join([x for x in fnt[1][:ind2] if x != "&" and x != " " and x != "\n"]) + "\n"
        af2_c_str = fhdr + af2_c_str

        af_u_str = ""
        for k, v in af_u.items():
            af_u_str += "{} {}\n".format(k, " ".join(v))
        fhdr = " ".join([x for x in fnt[0][:ind1] if x != "&" and x != " " and x != "\n"]) + "\n"
        af_u_str = u_hdr + af_u_str

        af_min_str = ""
        for k, v in af_min.items():
            af_min_str += "{} {}\n".format(k, " ".join(v))
        fhdr = " ".join([x for x in u_hdr if x != "&" and x != " " and x != "\n"]) + "\n"
        af_min_str = fhdr + af_min_str

        af_int_str = ""
        for k, v in af_int.items():
            af_int_str += "{} {}\n".format(k, " ".join(v))
        fhdr = " ".join([x for x in u_hdr if x != "&" and x != " " and x != "\n"]) + "\n"
        af_int_str = fhdr + af_int_str
        
        f_str = "AF1:\n{}\nAF2:\n{}\nComplement AF1:\n{}\nComplement AF2\n{}\nAF1&2 Union:\n{}\nMinimized AF:\n{}\nIntersection:\n{}\n".format(draw(fnt[0]),draw(fnt[1]),draw(af1_c_str),draw(af2_c_str),draw(af_u_str),draw(af_min_str),draw(af_int_str))
        print(f_str)

    def test_intersection_some(self):
        print('\nSome test\n')
        fnt = AF_SOME.split('---')
        af1_c, af2_c, u_hdr, af_u, af_min, af_int = af_intersection(AF_SOME.split('---'))
        u_hdr += '\n'
        ind1 = fnt[0].index("\n") + 1
        ind2 = fnt[1][1:].index("\n") + 1
        af1_c_str = ""

        for k, v in af1_c.items():
            af1_c_str += "{} {}\n".format(k, " ".join(v))
        fhdr = " ".join([x for x in fnt[0][:ind1] if x != "&" and x != " " and x != "\n"]) + "\n"
        af1_c_str = fhdr + af1_c_str

        af2_c_str = ""
        for k, v in af2_c.items():
            af2_c_str += "{} {}\n".format(k, " ".join(v))
        fhdr = " ".join([x for x in fnt[1][:ind2] if x != "&" and x != " " and x != "\n"]) + "\n"
        af2_c_str = fhdr + af2_c_str

        af_u_str = ""
        for k, v in af_u.items():
            af_u_str += "{} {}\n".format(k, " ".join(v))
        fhdr = " ".join([x for x in fnt[0][:ind1] if x != "&" and x != " " and x != "\n"]) + "\n"
        af_u_str = u_hdr + af_u_str

        af_min_str = ""
        for k, v in af_min.items():
            af_min_str += "{} {}\n".format(k, " ".join(v))
        fhdr = " ".join([x for x in u_hdr if x != "&" and x != " " and x != "\n"]) + "\n"
        af_min_str = fhdr + af_min_str

        af_int_str = ""
        for k, v in af_int.items():
            af_int_str += "{} {}\n".format(k, " ".join(v))
        fhdr = " ".join([x for x in u_hdr if x != "&" and x != " " and x != "\n"]) + "\n"
        af_int_str = fhdr + af_int_str
        
        f_str = "AF1:\n{}\nAF2:\n{}\nComplement AF1:\n{}\nComplement AF2\n{}\nAF1&2 Union:\n{}\nMinimized AF:\n{}\nIntersection:\n{}\n".format(draw(fnt[0]),draw(fnt[1]),draw(af1_c_str),draw(af2_c_str),draw(af_u_str),draw(af_min_str),draw(af_int_str))
        print(f_str)
    
    def test_intersection_diff_header(self):
        pass
        #intersec = src.af_intersection(AF1.split('---'))


if __name__ == "__main__":
    unittest.main()
